require('dotenv').config();
require('newrelic');
let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const passport = require('./config/passport');
let session = require ('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
let jwt = require('jsonwebtoken');

//Web routes
let indexRouter = require('./routes/index');
let bicicletasRouter = require('./routes/bicicletas');
let usuariosRouter = require('./routes/users');
let tokenRouter = require('./routes/token');
let authRouter = require('./routes/auth');

//Api routes
let bicicletasAPIRouter = require('./routes/api/bicicletas');
let usuariosAPIRouter = require('./routes/api/usuarios');
let authAPIRouter = require('./routes/api/auth');



let store;
if (process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(err){
    assert.ifError(error);
    assert.ok(false);
  });
}

let app = express();

app.set('secretkey', 'jwt_pwd_!!223344');

app.use(
  session({
    secret: 'wosoag!',
    cookie: { maxAge: 240 * 60 * 60 * 1000},
    store: store,
    saveUninitialized: true,
    resave: true,
    
  }),
);

const { get } = require('./mailer/mailer');


let mongoose = require('mongoose');
const { assert } = require('console');
//let mongoDB = 'mongodb://127.0.0.1/red_bicicletas';
//let mongoDB = 'mongodb+srv://Admin:r0vczhYM4sutR1nR@red-bicicletas.y3lv5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
let mongoDB = process.env.MONGO_URI
mongoose.connect(mongoDB, {useNewUrlParser: true})
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/login', authRouter);

app.use('/api/auth', authAPIRouter)
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

app.use('/privacy_policy', function(req, res){
  res.sendFile('public/policy_privacy.html');
});

app.get('/auth/google',
  passport.authenticate('google', {scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus/.profile.emails.read' 
  ]})
);

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/error'
  })
);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn (req, res, next){
  if(req.user) {
    next();
  } else {
    console.log('user sin logeo');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function(err, decoded){
    if(err) {
      res.json({status: "error", message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded );

      next();
    }
  });
}


module.exports = app;
