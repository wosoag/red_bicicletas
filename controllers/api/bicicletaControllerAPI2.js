let Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = function (req, res) {
    Bicicleta.find({}, function (err, bicicletas){
      res.status(200).json({
          bicicletas: Bicicleta.allBicis})
    });


};

exports.bicicleta_create = (req, res) => {
  let bici = new Bicicleta(req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];
  console.log(bici);
  Bicicleta.add(bici);
  res.status(200).json({
    bicicleta: bici
  });
};

exports.bicicleta_update = function (req, res) {
  var bici = Bicicleta.findById(req.body.id, function (err, targetBici){
    targetBici.color = req.body.color;
    targetBici.modelo = req.body.modelo;
    targetBici.ubicacion = [req.body.lat, req.body.lng];
    targetBici.save();

    res.status(200).json({
        bicicleta: targetBici
    });
  });
};

exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
};
