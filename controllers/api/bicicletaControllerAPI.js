const mongoose = require('mongoose');

let Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = function(req, res, next) {
    Bicicleta.find()
        .select("code color modelo ubicacion")
        .exec()
        .then(docs => {
            console.log(docs);

            const response ={
                count: docs.length,
                bicicletas: docs.map(doc => {
                    return {
                    _id: doc._id,
                    code: doc.code,
                    color: doc.color,
                    modelo: doc.modelo,
                    ubicacion: doc.ubicacion,
                    request: {
                        type: 'GET',
                        Url: 'http://localhost:3000/bicicletas/' + doc._id
                    }

                }
              })


            };

            if (docs.length  > 0) {
                res.status(200).json(response);
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }


        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

};


exports.bicicleta_create = function (req, res, next) {
    lat = parseFloat(req.body.lat)
    lng = parseFloat(req.body.lng)
    const bicicleta = new Bicicleta({
        _id: new mongoose.Types.ObjectId(),
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [lat, lng]
    });

    bicicleta
    .save()
    .then( result => {
        console.log(result);
        if(typeof(result.code)!="undefined" || typeof(result.color)!="undefined" || typeof(result.modelo)!="undefined"){
            res.status(201).json({
                message: 'Bicicleta agregada satisfactoriamente',
                createbicicleta: {
                    code: result.code,
                    color: result.color,
                    modelo: result.modelo,
                    ubicacion: result.ubicacion,
                    _id: result._id,
                    request: {
                        type: 'GET',
                        url: "http//localhost:3000/bicicleta/" + result._id
                    }
                }
            });
        } else {
            res.json({message: 'Por favor ingrese todos los campos'})
        }




    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        })
    });


};

exports.bicicleta_delete = function (req, res, next) {
    Bicicleta
    .deleteOne({code: req.body.code})
    .then( result => {
        console.log(result);
        if(typeof(result)!="undefined") {
            res.status(201).json({
                message: 'Bicicleta Eliminada satisfactoriamente',
            });
        } else {
            res.json({message: 'Por favor ingrese un codigo existente'})
        }
      
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error:err
        })
    });


};


exports.bicicleta_update = function (req, res, next) {
    lat = parseFloat(req.body.lat)
    lng = parseFloat(req.body.lng)
    console.log(req.body);
    console.log(req.params);  
    intCode = parseInt(req.body.code)
    filter = {code: `${intCode}`};
   
    let bici= {
        code: parseFloat(req.body.code),
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [lng, lat]
    };
    Bicicleta.findOneAndUpdate(filter,bici)
    .then(result => {
            console.log(result);
            if(typeof(result.code)!="undefined" || typeof(result.color)!="undefined" || typeof(result.modelo)!="undefined"){
                res.status(201).json({
                    message: 'Bicicleta editada satisfactoriamente',
                    createbicicleta: {
                        code: result.code,
                        color: result.color,
                        modelo: result.modelo,
                        ubicacion: result.ubicacion,
                        _id: result._id,
                        request: {
                            type: 'GET',
                            url: "http//localhost:3000/bicicleta/" + result._id
                        }
                    }
                });
            } else {
                res.json({message: 'Por favor ingrese todos los campos'})
            }
    
    
    
    
        })    
    .catch(err => {
      console.log(err);
      res.status(500).json({error: err});
    });

};
