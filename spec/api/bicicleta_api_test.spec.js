let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');
let server = require('../../bin/www')
let request = require('request');

let base_url = "http://127.0.0.1:3000/api/bicicletas";

describe('Bicicleta API', () => {
  beforeEach(function(done) {
    let mongoDB = 'mongodb://127.0.0.1/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser:true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console,'connection error'));
    db.once('open', function() {
      console.log('we are conected to test database');
      done();
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      mongoose.disconnect(err);
      done();
    });
  });
  
  describe('GET BICICLETAS / ', () => {
    it("Status 200", (done) => {
           
      request.get(base_url, function(err, response, body){
      console.log(response.statusCode);
      expect(response.statusCode).toBe(200);
      done();
      })
      
    
    });
  });


  describe("POST BICICLETAS /create", () => {
    it("Status 200", (done) => {
      
      let headers = {'content-type': 'application/json'};
      let aBici = '{"code": 10, "color": "rojo", "modelo":"urbana", "lng": -34.59, "lat"; 54.38}';
      request.post({
        headers: headers,
        url: 'http://127.0.0.1/api/bicicletas/create',
        body: aBici
      }, function(error, response, body){
        console.log(response);
        console.log(response.statusCode);
        expect(response.statusCode).toBe(201);
            
        expect(bici.color).toBe("rojo");
        expect(bici.ubicacion[0]).toBe(-34);
        expect(bici.ubicacion[1]).toBe(-54);
        
      });
      done();
    });
  });

});
