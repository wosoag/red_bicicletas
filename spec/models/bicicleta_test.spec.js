let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');



describe('Testing Bicicletas',  () => {
  beforeEach(function(done) {
    let mongoDB = 'mongodb://127.0.0.1/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser:true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console,'connection error'));
    db.once('open', function() {
      console.log('we are conected to test database');
      done();
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      mongoose.disconnect(err);
      done();
    });
  });


  describe('Bicicleta.allBicis', ()=> {
    it('comienza vacia', (done) => {
      Bicicleta.find(function(err, bicis) {
        expect(bicis.length).toBe(0);
        console.log(bicis)
        console.log(bicis.length);
        done();
      });
    });
  });


  describe('Bicicleta.add', () => {
    it('agrega solo una bici', (done) => {
      let aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
      aBici.save()
      .then(function (err, newBici){
        if (err) console.log(err);
        Bicicleta.find(function(err, bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);

          done();
        });
      });
    });
  });

  describe('Bicicleta .findbycode', ()=> {
    it('debe devolver la bici con code 1', (done) => {
      Bicicleta.find()
      .then(docs => {
        expect(docs.length).toBe(0);
        let aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
        aBici.save()
      
        .then(docs => {
          let aBici2 = new Bicicleta({code: 2, color: "rojo", modelo:"urbana"})
          aBici2.save()
      
          .then(docs =>{ 
            Bicicleta.findOne({code: 1})
            .select("code color modelo ubicacion _id")
      
            .then(docs => {
              expect(docs.code).toBe(aBici.code);
              expect(docs.color).toBe(aBici.color);
              expect(docs.modelo).toBe(aBici.modelo);

              done();
            })
          })
        })
      })  
  })
  })

  describe('Bicicleta.remove', () => {
    it('agrega solo una bici y la borra', (done) => {
      let aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
      aBici.save()
      .then(function (err, newBici){
        if (err) console.log(err);
        Bicicleta.find(function(err, bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);

          
        });
      });
        
        

        
        // Remove bici and test!
      const filter = parseInt(aBici.code);
      console.log({filter: filter});
      //Bicicleta.deleteMany({})
      Bicicleta.findOneAndDelete({code: 1})
        .then(result => {
          console.log({deleteResult :result});
        });

      Bicicleta.findOne({code: 1})
        .select("code color modelo ubicacion")
        .exec()
        .then(docs => {
            console.log({resultadoFind: docs});
            expect(docs.lenth).toBe(0);
        });
      done();
        
    });
  });



});


















