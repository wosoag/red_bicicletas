let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');
let Usuario = require('../../models/usuario');
let Reserva = require('../../models/reserva');

describe('Testing Usuarios', function() {
  beforeEach(function(done) {
    let mongoDB = 'mongodb://127.0.0.1/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', () => {
        console.log('we are connected to testdb');

        done();
      });
  });

  afterEach(function(done){
    Reserva.deleteMany({},function(err, success){
      if (err) console.log(err);
      Usuario.deleteMany({},function(err, success){
        if (err) console.log(err);
        Bicicleta.deleteMany({},function(err, success){
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe('Cuando un usuario reserva una bici', () => {
    it('debe existir la reserva', (done) => {
      const usuario = new Usuario({nombre: 'Ezequiel', password: "123456"});
      
      usuario.save()
        
      const bicicleta = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
      bicicleta.save()
        

      let hoy = new Date();
      let mañana = new Date();
      
      mañana.setDate(hoy.getDate()+1);
      usuario.reservar(bicicleta._id, hoy, mañana, function(err, reserva){
        Reserva.find().populate('bicicleta').populate('usuario').exec(function(err, reservas){
          console.log(reservas[0]);
          expect(reservas.length).toBe(1);
          expect(reservas[0].diasDeReserva()).toBe(2);
          console.log(reservas[0].diasDeReserva());
          expect(reservas[0].bicicleta.code).toBe(1);
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
          console.log(reservas[0].bicicleta.code);
          console.log(reservas[0].usuario.nombre);
          done();
        })
      }) 
      
       
      
        
    })
  });
  

});
