const passport = require ('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
const mongooge = require('mongoose');
const bcrypt = require('bcrypt');
const GoogleStrategy = require('passport-google-oauth20').Strategy
const FacebookTokenStrategy = require('passport-facebook-token');


passport.use(new FacebookTokenStrategy({
    clientID:process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
  }, function(accessToken, refreshToken, profile, done) {
    try {
      Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
        if (err) console.log('err' + err);
        return done(err, user);
      });
    }catch(err2){
      console.log(err2);
      return done(err2, null);
    }
  }
));



passport.use(new LocalStrategy(

    function verify(email, password, cb) {
        Usuario.findOne({ email: email }, function (err, user) {
            
            if (err) {return cb(err);}
            if (!user) {return cb(null, false, { message: 'Correo no existe, por favor verifiquelo' }); }
                     
            if (!bcrypt.compareSync(password, user.password)) {return cb(null, false, { message: 'Contraseña incorrecta' }); }
            
  
            return cb(null, user);
        });
    }
  ));
  
  passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
  },
    function(accessToken, refreshToken, profile, cb) {
      console.log(profile);

      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
      });
    })
  );


  
  passport.serializeUser(function(user, cb){
    cb(null, user.id);
  });
  
  passport.deserializeUser(function(id, cb) {
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
  });

module.exports = passport;