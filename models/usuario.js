let mongoose = require('mongoose');
let crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
let Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const mailer = require('../mailer/mailer');
const Token = require('./token');

let Schema = mongoose.Schema;


const validateEmail = function(email) {
  const re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  return re.test(email);
};


let usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    require: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    require: [true, 'El email es obligatorio'],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Por favor, ingrese un email valido'],
    match: [/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});



usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya esta con otro usuario'});

usuarioSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next()
});

usuarioSchema.methods.validatePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
  
  let reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta})
  console.log({userid: this._id});
  console.log({biciId});
  console.log({usuarioReserva: reserva});
  reserva.save(cb)

};

usuarioSchema.method.resetPassword = function(cb) {
  const token = new Token({_userid: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save(function(err){
    if (err) { return cb(err);}

    const mailOptions = {
      from: 'no-replay@red_bicicletas.com',
      to: email_destination,
      subject: ' Reseteo de password de cuenta',
      text: 'Hola, \n\n' + 'Por favor, para resetear el password de su cuenta haga click en ete link: \n' + 'http://localhost:4000' + '\/resetPassword\/' + token.token + '.\n'
    };
    mailer.sendMail(mailOptions, function (err) {
      if (err) { 
        console.log(err.message)
        return cb(err); 
      }

      console.log('Se he enviado un email para resetear el pasword' + email_destination + '.');
    });
    cb(null);
    
  });
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
  let token = new Token({_userid: this.id, token: crypto.randomBytes(16).toString('hex')});
  let email_destination = this.email;
  token.save(function(err) {
    if (err) { return console.log(err.message);}

    const mailOptions = {
      from: 'no-replay@red_bicicletas.com',
      to: email_destination,
      subject: ' Verificación de cuenta',
      text: 'Hola, \n\n' + 'Por favor, para verificar su cuenta haga click en ete link: \n' + 'http://localhost:4000' + '\/token/confirmation\/' + token.token + '.\n'
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) { return console.log(err.message); }

      console.log('Se he enviado un email de bienvenida a' + email_destination + '.');
    });
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      {'googleId': condition.id}, {'email': condition.emails[0].value}
    ]},(err, result) => {
      if (result) {
        callback(err, result)
      } else {
        console.log('--------- CONDITION ---------');
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || 'SIN NOMBRE';
        values.verificado = true;
        values.password = condition_json.etag;
        console.log('--------- VALUES ---------');
        console.log(values);
        self.create(values, (err, result)=> {
          if (err) {console.log(err);}
          return callback(err, result)
        })
      }
    })
};


usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
  const self = this;
  console.log(condition);
  self.findOne({
    $or: [
      {'facebookId': condition.id}, {'email': condition.emails[0].value}
    ]},(err, result) => {
      if (result) {
        callback(err, result)
      } else {
        console.log('--------- CONDITION ---------');
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || 'SIN NOMBRE';
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString('hex');
        console.log('--------- VALUES ---------');
        console.log(values);
        self.create(values, (err, result)=> {
          if (err) {console.log(err);}
          return callback(err, result)
        })
      }
    })
};



module.exports = mongoose.model('Usuario', usuarioSchema);
