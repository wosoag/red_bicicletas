let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema(
  {
   
    code: {
      type: Number,
      unique: true,
      required: true,
      trim: true
    },
    color: {
      type: String,
      required: true
    },
    modelo: {
      type: String,
      required: true
    },
    ubicacion: [{
      type: Object, 
      required: true,
      index: { type: '2dsphere', sparse: true}
    }]
  }
);


module.exports = mongoose.model('Bicicleta', bicicletaSchema);
